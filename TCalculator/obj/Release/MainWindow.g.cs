﻿#pragma checksum "..\..\MainWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "246B007595115EF0E5D68BDCDDFE9D17"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace TCalculator {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 16 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas design;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path back;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas _5;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path zero;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock zeroText;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path dot;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dotText;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas _4;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path seven;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sevenText;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path eight;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock eightText;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path nine;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock nineText;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path EqualsButton;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock equalsText;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas _3;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path four;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fourText;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path five;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock fiveText;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path six;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sixText;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path additionButton;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock additionText;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas _10;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path one;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock oneText;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path two;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock twoText;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path three;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock threeText;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path subtractionButton;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock subtractionText;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas _14;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path ACButton;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock AC;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path invertButton;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock invertText;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path divisionButton;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock divisionText;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path multiplicationButton;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock multiplicationText;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas итог;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path back1;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox inputField;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path close;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path cut;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path settings;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TCalculator;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TCalculator;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 4 "..\..\MainWindow.xaml"
            ((TCalculator.MainWindow)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.design = ((System.Windows.Controls.Canvas)(target));
            return;
            case 3:
            this.back = ((System.Windows.Shapes.Path)(target));
            return;
            case 4:
            this._5 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 5:
            this.zero = ((System.Windows.Shapes.Path)(target));
            
            #line 23 "..\..\MainWindow.xaml"
            this.zero.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.zero_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 6:
            this.zeroText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.dot = ((System.Windows.Shapes.Path)(target));
            
            #line 25 "..\..\MainWindow.xaml"
            this.dot.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.DotMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.dotText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this._4 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 10:
            this.seven = ((System.Windows.Shapes.Path)(target));
            
            #line 29 "..\..\MainWindow.xaml"
            this.seven.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.seven_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 11:
            this.sevenText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.eight = ((System.Windows.Shapes.Path)(target));
            
            #line 31 "..\..\MainWindow.xaml"
            this.eight.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.eight_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.eightText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.nine = ((System.Windows.Shapes.Path)(target));
            
            #line 33 "..\..\MainWindow.xaml"
            this.nine.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.nine_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 15:
            this.nineText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.EqualsButton = ((System.Windows.Shapes.Path)(target));
            
            #line 35 "..\..\MainWindow.xaml"
            this.EqualsButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.equalsMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 17:
            this.equalsText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this._3 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 19:
            this.four = ((System.Windows.Shapes.Path)(target));
            
            #line 39 "..\..\MainWindow.xaml"
            this.four.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.four_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 20:
            this.fourText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 21:
            this.five = ((System.Windows.Shapes.Path)(target));
            
            #line 41 "..\..\MainWindow.xaml"
            this.five.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.five_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 22:
            this.fiveText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 23:
            this.six = ((System.Windows.Shapes.Path)(target));
            
            #line 43 "..\..\MainWindow.xaml"
            this.six.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.six_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 24:
            this.sixText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 25:
            this.additionButton = ((System.Windows.Shapes.Path)(target));
            
            #line 45 "..\..\MainWindow.xaml"
            this.additionButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.additionMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 26:
            this.additionText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 27:
            this._10 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 28:
            this.one = ((System.Windows.Shapes.Path)(target));
            
            #line 50 "..\..\MainWindow.xaml"
            this.one.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.one_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 29:
            this.oneText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 30:
            this.two = ((System.Windows.Shapes.Path)(target));
            
            #line 53 "..\..\MainWindow.xaml"
            this.two.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.two_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 31:
            this.twoText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 32:
            this.three = ((System.Windows.Shapes.Path)(target));
            
            #line 55 "..\..\MainWindow.xaml"
            this.three.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.three_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 33:
            this.threeText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 34:
            this.subtractionButton = ((System.Windows.Shapes.Path)(target));
            
            #line 57 "..\..\MainWindow.xaml"
            this.subtractionButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.subtractionMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 35:
            this.subtractionText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 36:
            this._14 = ((System.Windows.Controls.Canvas)(target));
            return;
            case 37:
            this.ACButton = ((System.Windows.Shapes.Path)(target));
            
            #line 61 "..\..\MainWindow.xaml"
            this.ACButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.ACMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 38:
            this.AC = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 39:
            this.invertButton = ((System.Windows.Shapes.Path)(target));
            
            #line 63 "..\..\MainWindow.xaml"
            this.invertButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.invertMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 40:
            this.invertText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.divisionButton = ((System.Windows.Shapes.Path)(target));
            
            #line 65 "..\..\MainWindow.xaml"
            this.divisionButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.divisionMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 42:
            this.divisionText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 43:
            this.multiplicationButton = ((System.Windows.Shapes.Path)(target));
            
            #line 67 "..\..\MainWindow.xaml"
            this.multiplicationButton.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.multiplicationMouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 44:
            this.multiplicationText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 45:
            this.итог = ((System.Windows.Controls.Canvas)(target));
            return;
            case 46:
            this.back1 = ((System.Windows.Shapes.Path)(target));
            return;
            case 47:
            this.inputField = ((System.Windows.Controls.TextBox)(target));
            return;
            case 48:
            this.close = ((System.Windows.Shapes.Path)(target));
            return;
            case 49:
            this.cut = ((System.Windows.Shapes.Path)(target));
            return;
            case 50:
            this.settings = ((System.Windows.Shapes.Path)(target));
            return;
            case 51:
            this.TCalculator = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

