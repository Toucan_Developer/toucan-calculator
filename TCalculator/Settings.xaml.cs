﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TCalculator
{
    /// <summary>
    /// Логика взаимодействия для Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            //set saving color to path
            mainButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.MainButtonsColor.A, MainWindow.save.MainButtonsColor.R, MainWindow.save.MainButtonsColor.G, MainWindow.save.MainButtonsColor.B));
            ACButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.ACButtonsColor.A, MainWindow.save.ACButtonsColor.R, MainWindow.save.ACButtonsColor.G, MainWindow.save.ACButtonsColor.B));

            OperationsButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.OperationsButtonsColor.A, MainWindow.save.OperationsButtonsColor.R, MainWindow.save.OperationsButtonsColor.G, MainWindow.save.OperationsButtonsColor.B));

            equalsButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.equalsButtonsColor.A, MainWindow.save.equalsButtonsColor.R, MainWindow.save.equalsButtonsColor.G, MainWindow.save.equalsButtonsColor.B));

            InputBackground.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.InputBackgroundColor.A, MainWindow.save.InputBackgroundColor.R, MainWindow.save.InputBackgroundColor.G, MainWindow.save.InputBackgroundColor.B));

            TextColor.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MainWindow.save.TextColor.A, MainWindow.save.TextColor.R, MainWindow.save.TextColor.G, MainWindow.save.TextColor.B));
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Point p = new Point(e.GetPosition(this).X, e.GetPosition(this).Y);
                if (p.X > 240 && p.X < 253 &&
                    p.Y > 13 && p.Y < 25)
                    this.Close();
                //restore default color settings
                if (p.X > 191 && p.X < 204 &&
                   p.Y > 13 && p.Y < 25)
                {
                    MainWindow.save.MainButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FFF1F1F1");
                    MainWindow.save.ACButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FF7BEDB7");
                    MainWindow.save.OperationsButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FF7BB3ED");
                    MainWindow.save.equalsButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FFE99D27");
                    MainWindow.save.InputBackgroundColor = System.Drawing.ColorTranslator.FromHtml("#FFABC5DD");
                    MainWindow.save.TextColor = System.Drawing.ColorTranslator.FromHtml("#FF0A0A0A");
                    this.Close();
                }
                this.DragMove();
            }
            catch
            {

            }
        }
        //event left button down at path 
        private void mainButtons_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //choose color and set to save settings if user click "ok"
            System.Windows.Forms.ColorDialog MyDialog = new System.Windows.Forms.ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = false;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.

            MyDialog.AllowFullOpen = true;
            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Path temp = (Path)sender;
                switch (temp.Name)
                {
                    case "mainButtons":
                        MainWindow.save.MainButtonsColor = MyDialog.Color;
                        mainButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;
                    case "ACButtons":
                        MainWindow.save.ACButtonsColor = MyDialog.Color;
                        ACButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;
                    case "OperationsButtons":
                        MainWindow.save.OperationsButtonsColor = MyDialog.Color;
                        OperationsButtons.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;
                    case "equalsButton":
                        MainWindow.save.equalsButtonsColor = MyDialog.Color;
                        equalsButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;
                    case "InputBackground":
                        MainWindow.save.InputBackgroundColor = MyDialog.Color;
                        InputBackground.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;
                    case "TextColor":
                        MainWindow.save.TextColor = MyDialog.Color;
                        TextColor.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(MyDialog.Color.A, MyDialog.Color.R, MyDialog.Color.G, MyDialog.Color.B));
                        break;

                }

            }

        }
    }
}
