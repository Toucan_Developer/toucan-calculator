﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TCalculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string input = "";
        string operand0 = "";
        string operand1 = "";
        char operation;
        double result = 0;
        public static SaveSettings save = new SaveSettings();
        public MainWindow()
        {
            Stream streamOut;
            //load save settings
            FileInfo fs = new FileInfo("Settings.bin");
            if (fs.Exists == true)
            {
                try
                {
                    streamOut = new FileStream("Settings.bin", FileMode.Open, FileAccess.Read);
                    BinaryFormatter outFormatter = new BinaryFormatter();
                    save = (SaveSettings)outFormatter.Deserialize(streamOut);

                    streamOut.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            InitializeComponent();
            ChangeColor();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Point p = new Point(e.GetPosition(this).X, e.GetPosition(this).Y);
                if (p.X > 257 && p.X < 269 &&
                    p.Y > 13 && p.Y < 25)
                {//save settings and close window
                    Stream stm = new FileStream("Settings.bin", FileMode.Create, FileAccess.Write);
                    BinaryFormatter inFormatter = new BinaryFormatter();
                    inFormatter.Serialize(stm, save);
                    stm.Close();
                    this.Close();
                }

                if (p.X > 236 && p.X < 248 &&
                     p.Y > 13 && p.Y < 25)
                    this.WindowState = WindowState.Minimized;
                if (p.X > 212 && p.X < 224 &&
                     p.Y > 13 && p.Y < 25)
                {
                    Settings sett = new Settings();
                    sett.Owner = this;
                    sett.ShowDialog();
                    ChangeColor();
                }
                this.DragMove();
            }
            catch
            {

            }
        }
        void ChangeColor()
        {
            one.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            two.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            three.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            four.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            five.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            six.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            seven.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            eight.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            nine.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            zero.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));
            dot.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.MainButtonsColor.A, save.MainButtonsColor.R, save.MainButtonsColor.G, save.MainButtonsColor.B));

            ACButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.ACButtonsColor.A, save.ACButtonsColor.R, save.ACButtonsColor.G, save.ACButtonsColor.B));

            invertButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.OperationsButtonsColor.A, save.OperationsButtonsColor.R, save.OperationsButtonsColor.G, save.OperationsButtonsColor.B));
            divisionButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.OperationsButtonsColor.A, save.OperationsButtonsColor.R, save.OperationsButtonsColor.G, save.OperationsButtonsColor.B));
            multiplicationButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.OperationsButtonsColor.A, save.OperationsButtonsColor.R, save.OperationsButtonsColor.G, save.OperationsButtonsColor.B));
            subtractionButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.OperationsButtonsColor.A, save.OperationsButtonsColor.R, save.OperationsButtonsColor.G, save.OperationsButtonsColor.B));
            additionButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.OperationsButtonsColor.A, save.OperationsButtonsColor.R, save.OperationsButtonsColor.G, save.OperationsButtonsColor.B));

            EqualsButton.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.equalsButtonsColor.A, save.equalsButtonsColor.R, save.equalsButtonsColor.G, save.equalsButtonsColor.B));

            back1.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.InputBackgroundColor.A, save.InputBackgroundColor.R, save.InputBackgroundColor.G, save.InputBackgroundColor.B));

            inputField.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));

            oneText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            twoText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            threeText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            fourText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            fiveText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            sixText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            sevenText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            eightText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            nineText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            zeroText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            dotText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));

            AC.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));

            invertText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            divisionText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            multiplicationText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            subtractionText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            additionText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
            equalsText.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(save.TextColor.A, save.TextColor.R, save.TextColor.G, save.TextColor.B));
        }




        #region Click at number panel
        private void one_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            inputField.Text = "";
            input += "1";
            ParseInput();
        }

        private void two_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "2";
            ParseInput();
        }

        private void three_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "3";
            ParseInput();
        }

        private void four_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "4";
            ParseInput();
        }

        private void five_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "5";
            ParseInput();
        }

        private void ParseInput()
        {
            double num1;
            double.TryParse(input, out num1);
            input = num1.ToString();
            inputField.Text = input;
        }

        private void six_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "6";
            ParseInput();
        }

        private void seven_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "7";
            ParseInput();
        }

        private void eight_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "8";
            ParseInput();
        }

        private void nine_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "9";
            ParseInput();
        }

        private void zero_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += "0";
            ParseInput();
        }
        private void DotMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            inputField.Text = "";
            input += ",";
            inputField.Text = input;
        }
        #endregion
        //clear input
        private void ACMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.inputField.Text = "";
            this.input = string.Empty;
            this.operand0 = string.Empty;
            this.operand1 = string.Empty;
        }


        #region mathematics operation
        private void additionMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            operand0 = input;
            operation = '+';
            input = string.Empty;
        }

        private void subtractionMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            operand0 = input;
            operation = '-';
            input = string.Empty;
        }

        private void multiplicationMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            operand0 = input;
            operation = '*';
            input = string.Empty;
        }

        private void divisionMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            operand0 = input;
            operation = '/';
            input = string.Empty;
        }
        private void invertMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            double num1;
            double.TryParse(input, out num1);
            num1 *= (-1);
            inputField.Text = "";
            input = num1.ToString();
            inputField.Text = input;
        }
        #endregion
        //get results
        private void equalsMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            operand1 = input;
            double num1, num2;
            double.TryParse(operand0, out num1);
            double.TryParse(operand1, out num2);

            if (operation == '+')
            {
                result = num1 + num2;
                inputField.Text = result.ToString();
            }
            else if (operation == '-')
            {
                result = num1 - num2;
                inputField.Text = result.ToString();
            }
            else if (operation == '*')
            {
                result = num1 * num2;
                inputField.Text = result.ToString();
            }
            else if (operation == '/')
            {
                if (num2 != 0)
                {
                    result = num1 / num2;
                    inputField.Text = result.ToString();
                }
                else
                {
                    inputField.Text = "DIV/Zero!";
                }

            }
            if (inputField.Text != "DIV/Zero!")
            {
                input = inputField.Text;
                operand0 = input;
            }
        }






    }
}
