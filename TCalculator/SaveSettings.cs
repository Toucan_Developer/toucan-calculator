﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCalculator
{
    [Serializable]
    public class SaveSettings
    {
        public System.Drawing.Color MainButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FFF1F1F1");
        public System.Drawing.Color ACButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FF7BEDB7");
        public System.Drawing.Color OperationsButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FF7BB3ED");
        public System.Drawing.Color equalsButtonsColor = System.Drawing.ColorTranslator.FromHtml("#FFE99D27");
        public System.Drawing.Color InputBackgroundColor = System.Drawing.ColorTranslator.FromHtml("#FFABC5DD");
        public System.Drawing.Color TextColor = System.Drawing.ColorTranslator.FromHtml("#FF0A0A0A");
    }
}
